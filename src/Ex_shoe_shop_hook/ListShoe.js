import React from "react";
import ItemShoe from "./ItemShoe";
export default function ListShoe({ list, handleAddToCart }) {
  return (
    <div className="row">
      {list.map((shoe) => {
        return <ItemShoe handleOnclick={handleAddToCart} item={shoe} />;
      })}
    </div>
  );
}
