import React from "react";

export default function Cart(props) {
  const { cart, onCartChange } = props;

  const handleIncreaseQuantity = (item) => {
    let cloneCart = [...cart];
    let index = cloneCart.findIndex((i) => i.id === item.id);
    cloneCart[index].soLuong++;
    onCartChange(cloneCart);
  };

  const handleDecreaseQuantity = (item) => {
    let cloneCart = [...cart];
    let index = cloneCart.findIndex((i) => i.id === item.id);
    if (cloneCart[index].soLuong > 1) {
      cloneCart[index].soLuong--;
    } else {
      cloneCart.splice(index, 1);
    }
    onCartChange(cloneCart);
  };

  const handleRemoveFromCart = (item) => {
    let cloneCart = [...cart];
    let index = cloneCart.findIndex((cartItem) => {
      return cartItem.id === item.id;
    });
    if (index !== -1) {
      cloneCart.splice(index, 1);
    }
    onCartChange(cloneCart);
  };

  const renderTbody = () => {
    return cart.map((item) => {
      return (
        <tr key={item.id}>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price * item.soLuong}</td>
          <td>
            <img src={item.image} alt="" style={{ width: 50 }} />
          </td>
          <td>
            <button
              className="btn btn-danger"
              onClick={() => handleDecreaseQuantity(item)}
            >
              -
            </button>
            <strong className="mx-3">{item.soLuong}</strong>
            <button
              className="btn btn-success"
              onClick={() => handleIncreaseQuantity(item)}
            >
              +
            </button>
          </td>
          <td>
            <button
              className="btn btn-danger"
              onClick={() => handleRemoveFromCart(item)}
            >
              Xóa
            </button>
          </td>
        </tr>
      );
    });
  };

  return (
    <div>
      <table className="table">
        <thead>
          <tr>
            <th>ID </th>
            <th>name</th>
            <th>Price</th>
            <th>Img</th>
            <th>Quantity</th>
            <th>Thao tác</th>
          </tr>
        </thead>
        <tbody>{renderTbody()}</tbody>
      </table>
    </div>
  );
}
