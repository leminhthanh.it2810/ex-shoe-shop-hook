import React from "react";

export default function ItemShoe(props) {
  const { image, name, price, description } = props.item;

  const handleClick = () => {
    props.handleOnclick(props.item);
  };
  return (
    <div className="col-3 p-4">
      <div className="card ">
        <img src={image} className="card-img-top" alt="..." />
        <div className="card-body">
          <h5 className="card-title">{name}</h5>
          <p className="card-text">
            Price: {price}
            <br />
            Description: {description}
          </p>
          <button onClick={handleClick} className="btn btn-primary">
            Add to cart
          </button>
        </div>
      </div>
    </div>
  );
}
